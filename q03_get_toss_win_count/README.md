# In our dataset, in how many matches the team 'Mumbai Indians' has won the toss?

Well we have done good so far!!

Do you know why 'Mumbai Indians' has won the most number of IPL seasons so far?
Mumbai Indians loves chasing as they have some excellent finishers from across the globe in their team.
Being so strong while chasing, Mumbai Indians stress a lot on winning the toss.

Their ideology is simple: 

Win the TOSS and win the MATCH

One way the team can optimise its performance is by finding the number of tosses won by the team.

## Write a function `get_toss_win_count` that :
- Work on the previously created `ipl_matches_array` object

## PARAMETERS
- Accept one optional parameter named `team`, with default value "Mumbai Indians"
- `type : str`

## RETURNS
- Should return an integer count of all the matches for which the given team has won the toss
- `type : int`
